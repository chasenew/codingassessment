package com.chase.codingchallenge.view.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.chase.codingchallenge.R


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.container)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction().setReorderingAllowed(true)
                .add(R.id.fragment_container_view, SchoolListView::class.java, null).commit()
        }

    }

}