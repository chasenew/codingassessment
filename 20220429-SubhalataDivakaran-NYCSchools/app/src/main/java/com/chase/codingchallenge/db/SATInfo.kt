package com.chase.codingchallenge.db

import androidx.room.Entity
import androidx.room.PrimaryKey

/***
 * This defines the SATInfo entity and defines the Table structure and primary key for the table.
 */
@Entity
data class SATInfo(
    @PrimaryKey
    val dbn: String,
    val mathScore: String?,
    val writingScore:String?,
    val readingScore:String?
)