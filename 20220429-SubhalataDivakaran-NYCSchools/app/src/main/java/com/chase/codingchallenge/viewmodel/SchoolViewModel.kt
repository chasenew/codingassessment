package com.chase.codingchallenge.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.chase.codingchallenge.api.SchoolApi
import com.chase.codingchallenge.model.SchoolInfo
import com.chase.codingchallenge.repository.SchoolRepository

/***
 * A lifecycle aware component that is used to manage the School Info as needed by the view
 */
class SchoolViewModel():ViewModel() {
    private lateinit var schoolRepo: SchoolRepository
    var schoolList = MutableLiveData<List<SchoolInfo>>()
    fun getAllSchools(): LiveData<List<SchoolInfo>?> {
        return schoolList
    }

    fun init( schoolApi:SchoolApi) {
        schoolRepo = SchoolRepository(schoolApi)
        schoolList = schoolRepo.fetchSchoolInfo()
    }
}