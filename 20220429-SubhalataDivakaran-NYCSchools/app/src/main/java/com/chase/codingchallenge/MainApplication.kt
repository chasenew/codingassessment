package com.chase.codingchallenge

import android.app.Application
import com.chase.codingchallenge.db.AppDatabase

/***
 * This class provides as global context and is used to initialise Database at the very beginning.
 */
class MainApplication : Application() {

    companion object {
        var db: AppDatabase? = null
        fun getAppDatabase(): AppDatabase? {
            return db
        }

    }
    override fun onCreate() {
        super.onCreate()
        db= AppDatabase.getDatabase(this)
    }

}