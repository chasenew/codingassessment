package com.chase.codingchallenge.view.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.chase.codingchallenge.R
import com.chase.codingchallenge.api.SchoolApi
import com.chase.codingchallenge.helper.ItemClickListener
import com.chase.codingchallenge.model.SchoolInfo
import com.chase.codingchallenge.view.adapter.SchoolListAdapter
import com.chase.codingchallenge.viewmodel.SchoolViewModel


class SchoolListView : Fragment(), ItemClickListener<SchoolInfo> {
    companion object {
        private const val TAG: String = "SchoolListView"
    }

    private lateinit var linearLayoutManager: LinearLayoutManager
    private var recyclerView: RecyclerView? = null
    private var schoolAdapter: SchoolListAdapter? = null
    private var errorMessage: TextView? = null
    private var progress: ProgressBar?=null


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view: View = inflater.inflate(R.layout.activity_main, container, false)
        errorMessage = view.findViewById(R.id.errorMessage)
        progress=view.findViewById(R.id.progressBar)
        renderData(view)
        return view
    }

    private fun renderData(view: View) {
        progress?.visibility=View.VISIBLE
        recyclerView = view.findViewById(R.id.schoolListing)
        linearLayoutManager = LinearLayoutManager(activity)
        recyclerView?.layoutManager = linearLayoutManager
        schoolAdapter = SchoolListAdapter()
        recyclerView?.adapter = schoolAdapter
        handleViewModel()
        if (schoolAdapter != null) {
            schoolAdapter!!.setOnItemClickListener(this)
        }

    }

    private fun handleViewModel(){
        val schoolVM = ViewModelProvider(this)[SchoolViewModel::class.java]
        schoolVM.init(SchoolApi.getInstance())
        schoolVM.getAllSchools()?.observe(this,
            { results ->
                progress?.visibility=View.GONE
                if (schoolAdapter != null) {
                    if (results != null) {
                        schoolAdapter!!.setData(results)
                        errorMessage?.visibility = View.GONE
                    } else {
                        Log.w(TAG, "No Results Found")
                        showGenericError()
                    }
                } else {
                    Log.e(TAG, "School Adapter is null")
                    showGenericError()
                }
            })
    }
    /***
     * Defining the onClick function which replaces the new fragment and passes the identifier
     * dbn to fetch the SAT score for the specific school that was clicked
     */
    override fun onClick(view: View?, data: SchoolInfo, position: Int) {
        Log.i(
            TAG, "School ref:${data.dbn} School name:${data.school_name}"
        )
        val mFrag = SATDetails()
        val bundle = Bundle()
        bundle.putString("DBN", data.dbn) //parameters are (key, value).
        mFrag.arguments = bundle
        val fragmentTransaction: FragmentTransaction? =
            activity?.supportFragmentManager?.beginTransaction()
        fragmentTransaction?.replace(
            R.id.fragment_container_view,
            mFrag,
            SATDetails.TAG
        )?.addToBackStack(null)
        fragmentTransaction?.commit()

    }

    /***
     * This is to show a generic error to end user when the data cannot be displayed
     */
    private fun showGenericError() {
        if(context!=null) {
            errorMessage?.text = context!!.getText(R.string.failure_msg_school)
        }
        errorMessage?.visibility = View.VISIBLE
        progress?.visibility=View.GONE

    }

}