package com.chase.codingchallenge.helper

import android.view.View

/***
 * This is an interface to introduces Item Click Listener in the Recyclerview
 */
interface ItemClickListener<T> {
    fun onClick(view: View?, data: T, position: Int)
}