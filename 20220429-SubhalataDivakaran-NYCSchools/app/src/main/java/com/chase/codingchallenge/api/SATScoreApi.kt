package com.chase.codingchallenge.api

import com.chase.codingchallenge.Constants
import com.chase.codingchallenge.model.SATScores
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET

/***
 * This is a retrofit api interface to invoke the SAT Scores Api and parse it using Moshi for Json Parsing
 * For score info
 */
interface SATScoreApi {
    @GET("f9bf-2cp4.json")
    fun getSATScores(): Call<List<SATScores>>

    companion object {
        private var scoreApi: SATScoreApi? = null
        fun getInstance() : SATScoreApi {
            if (scoreApi == null) {
                val retrofit = Retrofit.Builder()
                    .baseUrl(Constants.BASE_URL)
                    .addConverterFactory(MoshiConverterFactory.create())
                    .build()
                scoreApi = retrofit.create(SATScoreApi::class.java)
            }
            return scoreApi!!
        }
    }
}