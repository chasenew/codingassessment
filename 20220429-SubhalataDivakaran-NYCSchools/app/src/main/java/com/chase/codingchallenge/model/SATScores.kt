package com.chase.codingchallenge.model

/***
 * Data class with essential fields to be parsed and displayed on UI
 */
data class SATScores(
    val sat_critical_reading_avg_score: String,
    val sat_math_avg_score: String, val sat_writing_avg_score: String,
    val dbn: String
)
