package com.chase.codingchallenge.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.chase.codingchallenge.R
import com.chase.codingchallenge.helper.ItemClickListener
import com.chase.codingchallenge.model.SchoolInfo

/***
 * This is to inflate and assign values to each of the items in the list
 */
class SchoolListAdapter():
    RecyclerView.Adapter<SchoolListAdapter.ListViewHolder>()
{
    private var schools: List<SchoolInfo>? = null
    private var itemClickListener: ItemClickListener<SchoolInfo>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        return ListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.itemview, parent,false))
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        holder.tvSchoolName.text = schools?.get(position)?.school_name?:""
        holder.tvSchoolName.tag=schools?.get(position)?.dbn?:""
        holder.itemView.setOnClickListener {
            schools?.get(position)?.let { it1 -> itemClickListener?.onClick(it, it1,  position) }
        }

    }

    override fun getItemCount(): Int {
        return schools?.size ?: 0
    }

    class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val tvSchoolName: TextView= itemView.findViewById(R.id.schoolName)

    }

    fun setOnItemClickListener(itemClickListener:ItemClickListener<SchoolInfo>){
        this.itemClickListener = itemClickListener
    }

    /**
     * This function is used to set the data. As an enhancement I would notify only if new data is added.
     */
    fun setData(schoolList:List<SchoolInfo>?) {
        schools = schoolList
        //add more specific data set change as improvement
        notifyDataSetChanged()
    }

}