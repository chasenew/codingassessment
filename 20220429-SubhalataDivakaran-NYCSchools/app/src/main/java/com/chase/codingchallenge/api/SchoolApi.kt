package com.chase.codingchallenge.api

import com.chase.codingchallenge.Constants
import com.chase.codingchallenge.model.SchoolInfo
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET


/***
 * This is a retrofit api interface to invoke the NYC School data and parse it using Moshi for Json Parsing
 */
interface SchoolApi {
        @GET("s3k6-pzi2.json")
        fun getSchoolList(): Call<List<SchoolInfo>>

        companion object {
                private var schoolApi: SchoolApi? = null

                fun getInstance() : SchoolApi {
                        if (schoolApi == null) {
                                val retrofit = Retrofit.Builder()
                                        .baseUrl(Constants.BASE_URL)
                                        .addConverterFactory(MoshiConverterFactory.create())
                                        .build()
                                schoolApi = retrofit.create(SchoolApi::class.java)
                        }
                        return schoolApi!!
                }
        }

}