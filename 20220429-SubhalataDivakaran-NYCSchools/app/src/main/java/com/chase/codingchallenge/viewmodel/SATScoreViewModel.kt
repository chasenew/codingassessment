package com.chase.codingchallenge.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.chase.codingchallenge.api.SATScoreApi
import com.chase.codingchallenge.db.SATInfo
import com.chase.codingchallenge.model.SATScores
import com.chase.codingchallenge.repository.SATScoreRepo

/***
 * A lifecycle aware component that is used to manage the SATScores as needed by the view
 */
class SATScoreViewModel() : ViewModel() {
    private lateinit var scoreRepo: SATScoreRepo
    var satScores = MutableLiveData<List<SATScores>>()
    fun getAllScores(): LiveData<List<SATScores>?> {
        return satScores
    }

    fun init(scoreApi: SATScoreApi) {
        scoreRepo = SATScoreRepo(scoreApi)
        satScores = scoreRepo.fetchSATScores()
        scoreRepo.setDAOInstance()
    }


    fun insert(satInfo: SATInfo) {
        scoreRepo.insert(satInfo)
    }

    fun getSatScore(dbn: String): LiveData<SATInfo>? {
        return scoreRepo.getScore(dbn)
    }


}