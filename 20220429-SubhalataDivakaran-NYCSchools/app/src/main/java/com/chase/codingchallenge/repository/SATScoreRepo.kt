package com.chase.codingchallenge.repository

import android.app.Application
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.chase.codingchallenge.MainApplication
import com.chase.codingchallenge.api.SATScoreApi
import com.chase.codingchallenge.db.AppDatabase
import com.chase.codingchallenge.db.SATDao
import com.chase.codingchallenge.db.SATInfo
import com.chase.codingchallenge.model.SATScores
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/***
 * This is the repository for SAT score which abstracts the data source
 */
class SATScoreRepo(private val apiService: SATScoreApi) {

    private val scoreLiveData: MutableLiveData<List<SATScores>> by lazy {
        MutableLiveData<List<SATScores>>()
    }
    private val scoreFromDB: MutableLiveData<SATInfo> by lazy {
        MutableLiveData<SATInfo>()
    }
    private var satDAO:SATDao?=null

    companion object {
        private const val TAG: String = "SATScoreRepo"
        private var db:AppDatabase?=null
         }

    fun setDAOInstance() {
            db =MainApplication.getAppDatabase()
            satDAO = db?.satDao()
    }


    fun fetchSATScores(): MutableLiveData<List<SATScores>> {
        apiService.getSATScores().enqueue(object : Callback<List<SATScores>> {
            override fun onResponse(
                call: Call<List<SATScores>>,
                response: Response<List<SATScores>>
            ) {
                if (response.isSuccessful) {
                    scoreLiveData.value = response.body()
                } else {
                    scoreLiveData.value=null
                    Log.w(TAG, "Didn't receive a valid response")
                }
            }

            override fun onFailure(call: Call<List<SATScores>>, t: Throwable) {
                Log.e(TAG, "School Api failed!!")
                scoreLiveData.value=null
            }

        })
        return scoreLiveData
    }

    fun insert(satInfo: SATInfo) {
        AppDatabase.databaseWriteExecutor.execute {
            satDAO?.insertAll(satInfo)
        }
    }


    fun getScore(dbn:String): LiveData<SATInfo>?{
        return satDAO?.getItemById(dbn)
    }


}