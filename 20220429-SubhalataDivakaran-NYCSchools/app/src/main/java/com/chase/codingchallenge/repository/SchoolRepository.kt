package com.chase.codingchallenge.repository


import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.chase.codingchallenge.api.SchoolApi
import com.chase.codingchallenge.model.SchoolInfo
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/***
 * This is the repository for Schools which abstracts the data source which provides the school details, in this case API
 */
class SchoolRepository(private val apiService: SchoolApi) {
    companion object {
        private const val TAG: String = "SchoolRepository"
    }

    private val scoreLiveData: MutableLiveData<List<SchoolInfo>> by lazy {
        MutableLiveData<List<SchoolInfo>>()
    }

    fun fetchSchoolInfo(): MutableLiveData<List<SchoolInfo>> {
        apiService.getSchoolList().enqueue(object : Callback<List<SchoolInfo>> {
            override fun onResponse(
                call: Call<List<SchoolInfo>>,
                response: Response<List<SchoolInfo>>
            ) {
                if(response.isSuccessful) {
                    scoreLiveData.postValue(response.body())
                }else{
                    scoreLiveData.value=null
                    Log.w(TAG, "Didnt receive a valid response")
                }
            }


            override fun onFailure(call: Call<List<SchoolInfo>>, t: Throwable) {
                Log.e(TAG, "School Api failed!!!!")
                scoreLiveData.value=null
            }

        })
        return scoreLiveData
    }


}