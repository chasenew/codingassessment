package com.chase.codingchallenge.db

import androidx.lifecycle.LiveData
import androidx.room.*

/***
 *
 *The is the Data access object to handle all CRUD operations on SATInfo table.
 */
@Dao
interface SATDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg satInput: SATInfo)

    @Delete
    fun delete(satInfo: SATInfo)

    @Query("SELECT * FROM SATInfo WHERE dbn=:dbn")
    fun getItemById(dbn:String): LiveData<SATInfo>

//    @Query("SELECT * FROM SATInfo")
//    fun getAll(): List<SATInfo>
}

