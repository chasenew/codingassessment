package com.chase.codingchallenge.view.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModelProvider
import com.chase.codingchallenge.R
import com.chase.codingchallenge.api.SATScoreApi
import com.chase.codingchallenge.db.SATInfo
import com.chase.codingchallenge.model.SATScores
import com.chase.codingchallenge.viewmodel.SATScoreViewModel

class SATDetails : Fragment() {
    companion object {
        const val TAG: String = "SATDetails"
    }
    private var satScoreList = HashMap<String, SATScores>()
    private var dbn: String? = null
    private var labelReadingScore: TextView? = null
    private var labelWritingScore: TextView? = null
    private var labelMathScore: TextView? = null
    private var errorMessage: TextView? = null
    private var progress: ProgressBar? = null
    private var tvReadingScore: TextView? = null
    private var tvWritingScore: TextView? = null
    private var tvMathScore: TextView? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(
            R.layout.sat_score_view, container, false
        )
        initView(view)
        return view
    }
    private fun initView(view: View) {
        tvReadingScore = view.findViewById(R.id.tv_sat_critical_reading_avg_score)
        tvWritingScore = view.findViewById(R.id.tv_sat_writing_avg_score)
        tvMathScore = view.findViewById(R.id.tv_sat_math_avg_score)
        labelReadingScore = view.findViewById(R.id.sat_critical_reading_avg_score)
        labelWritingScore = view.findViewById(R.id.sat_writing_avg_score)
        labelMathScore = view.findViewById(R.id.sat_math_avg_score)
        errorMessage = view.findViewById(R.id.sat_error_msg)
        progress = view.findViewById(R.id.satProgressBar)
        hideViews()
        progress?.visibility = View.VISIBLE
        val bundle = arguments
        if (bundle != null) {
            dbn = bundle.getString("DBN")
            Log.d(
                TAG, "Bundle argument:${dbn}"
            )
            handleViewModel()

        } else {
            Log.e(TAG, "No Arguments passed")
            showGenericError()
        }
    }

    /***
     * This function is used to observe the ViewModel for the data needed for the UI
     */
    private fun handleViewModel() {
        val satScoreVM: SATScoreViewModel =
            ViewModelProvider(this)[SATScoreViewModel::class.java]
        satScoreVM.init(SATScoreApi.getInstance())
        satScoreVM.getAllScores()?.observe(this, { results ->
            satScoreList.clear()
            if (results != null) {
                progress?.visibility = View.GONE
                for (result in results.iterator()) {
                    satScoreList[result.dbn] = result
                    val satInfo = SATInfo(
                        result.dbn, result.sat_math_avg_score, result.sat_writing_avg_score,
                        result.sat_critical_reading_avg_score
                    )
                    satScoreVM.insert(satInfo)
                }
                getDatafromDB(satScoreVM)//saving data in RoomDatabase and using query to fetch the data
//                getDataFromHashMap() Using hashmap to fetch data

            } else {
                Log.e(TAG, "Results are null")
                showGenericError()
            }


        })
    }

    /***
     * This function gets the record from the Room database and sets in the UI
     * Given more time i would have refactored this class and use Cooroutines to handle data fetching
     */
    private fun getDatafromDB(satScoreVM: SATScoreViewModel) {
        if (dbn != null) {
            val record1 = satScoreVM.getSatScore(dbn!!) as LiveData<SATInfo>
            record1
                .observe(
                    activity!!,
                    { satInfo: SATInfo? ->
                        showViews()
                        if (satInfo != null) {
                            setData(satInfo)
                        } else {
                            Log.e(TAG, "Record is null")
                            showGenericError()
                        }
                    })
        } else {
            Log.e(TAG, "dbn is missing")
            showGenericError()
        }
    }

    /***
     * This function gets the record from an in-memory HashMap and sets in the UI
     */
    private fun getDataFromHashMap() {
        if (!satScoreList.isNullOrEmpty() && dbn != null) {
            val record = satScoreList.get(dbn)
            if (record != null) {
                showViews()
                val satInfo = SATInfo(
                    record.dbn,
                    record.sat_math_avg_score,
                    record.sat_writing_avg_score,
                    record.sat_critical_reading_avg_score
                )
                setData(satInfo)
            } else {
                Log.e(TAG, "Record is null")
                showGenericError()
            }
        } else {
            Log.e(TAG, "Score list is null or dbn is missing")
            showGenericError()
        }
    }

    /***
     * This function is used to set data on the invidual textviews
     */
    private fun setData(satInfo: SATInfo?) {
        tvReadingScore?.text = satInfo?.readingScore
        tvWritingScore?.text = satInfo?.writingScore
        tvMathScore?.text = satInfo?.mathScore
    }

    /***
     * This function is used to hide views when there is an error or loading in progress
     */
    private fun hideViews() {
        labelReadingScore?.visibility = View.GONE
        labelWritingScore?.visibility = View.GONE
        labelMathScore?.visibility = View.GONE

    }

    /***
     * This function is used to show views when there is an proper response
     */
    private fun showViews() {
        labelReadingScore?.visibility = View.VISIBLE
        labelWritingScore?.visibility = View.VISIBLE
        labelMathScore?.visibility = View.VISIBLE

    }
    /***
     * This is to show a generic error to end user when the data cannot be displayed
     */
    private fun showGenericError() {
        if (context != null) {
            errorMessage?.text = context!!.getText(R.string.failure_msg_score)
        }
        errorMessage?.visibility = View.VISIBLE
        progress?.visibility = View.GONE
        hideViews()
    }
}