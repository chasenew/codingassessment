package com.chase.codingchallenge.model

/***
 * Data class with essential fields to be parsed and displayed on UI
 * for School info
 */
data class SchoolInfo(val dbn:String,val school_name:String)
